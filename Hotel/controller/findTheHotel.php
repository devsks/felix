<html>
<body>
<?php
ini_set('display_errors', 1);

require '../source/db.connect.inc.php';

$servername = MYSQL_HOST;
$username = MYSQL_USER;
$password = MYSQL_PASSWORD;
$db = MYSQL_DB;

// Connecting to database	
try {

    $conn = new PDO("mysql:host=$servername;dbname=$db", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "Connection Sucessfull ";
	// Checking for empty query
	if(isset($_GET["divX"]) )
    {	   
        $lat = htmlspecialchars(trim($_GET["divX"]));
        $log = htmlspecialchars(trim($_GET["divY"]));
       if(isset($_GET["rad"]))
            $rad = htmlspecialchars(trim($_GET["rad"]));
        else
            $rad = 10;
		$rows = $conn->query("SELECT room_id,hotel_name,room_price,rating,map_location FROM hotel INNER JOIN room on hotel.hotel_id = room.hotel_id 
		where SQRT( POW(longitude -".$lat.",2)".
		" + POW(latitude - ".$log.",2) ) <=".$rad .";");
		$data_retun="";
		echo '	<table>
				<thead>
				<tr>
					
					<th>Room ID</th>
					<th>Hotel Name</th>
					<th>Price</th>
					<th>Rating</th>
					<th>Location</th>
				</tr>
			</thead>
			<tbody>';	
		foreach($rows as $row )
		{
			$data_retun.="<tr>";
			$data_retun.="<td>$row[0]</td>";
			$data_retun.="<td>$row[1]</td>";
			$data_retun.="<td>$row[2]</td>";
			$data_retun.="<td>$row[3]</td>";
			$data_retun.="<td>$row[4]</td>";
			$data_retun.="</tr>";
		}
		//	echo $row[0],$row[1],$row[2],$row[3],$row[4]."<br>";
		echo $data_retun."</tbody></table>";
	}
    else
		echo "Connection failed: ";
}   
catch(PDOException $e)
{
	echo "Connection failed: " . $e->getMessage();
}
?>
</body>
</html>